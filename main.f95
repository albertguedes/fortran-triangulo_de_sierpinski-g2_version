  PROGRAM triangulo_de_sierpinsk

    use library

    real v(3,2), color(3)

    real x,y

    real dev, d

    integer i

    v(1,1) = 0.0
    v(1,2) = 0.0

    v(2,1) = 300.0
    v(2,2) = 600.0

    v(3,1) = 600.0
    v(3,2) = 0.0

    color(1) = 3 ! azul
    color(2) = 7 ! verde
    color(3) = 19 ! vermelhor

    dev = g2_open_vd_()
    d = g2_open_x11_(600.0,600.0)

    call g2_attach_(dev,d)

    call g2_pen_(d,1.0)

    x = 600*random()
    y = 600*random()
    do while(.true.)

        i = 1 + 3*random()

        x = ( x + v(i,1) )/2.0
        y = ( y + v(i,2) )/2.0

        call g2_pen_(d,color(i))
        call g2_plot_(d, x, y )
        call g2_flush_(d)

    enddo

    call g2_close_(d)
    call g2_close_(dev)

  END PROGRAM triangulo_de_sierpinsk
